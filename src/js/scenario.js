/** @type {Jeu} jeu */
let jeu;  // variable globale représentant le jeu actuel

document.addEventListener("keydown", function (event) {
    switch (event.key) {
        case 'ArrowLeft':
            // déplacement vers la gauche
            jeu.prota.deplacer(0, -1);
            break;
        case 'ArrowUp':
            // déplacement vers le haut
            jeu.prota.deplacer(-1, 0);
            break;
        case 'ArrowRight':
            // déplacement vers la droite
            jeu.prota.deplacer(0, 1);
            break;
        case 'ArrowDown':
            // déplacement vers le bas
            jeu.prota.deplacer(1, 0);
            break;
        default:
    }

    miseAJour();
});


/**
 * Met à jour la partie et l'affichage pour le joueur en fonction de la position du joueur
 * - indique si la partie est gagnée ou perdue
 * - indique le nombre de mines à proximité du joueur
 * - affiche le score du joueur
 * - met à jour l'image représentant le joueur
 */
function miseAJour() {
    const score = document.getElementById("score");
    const msg = document.getElementById("message");
    const nbMines = jeu.nbMinesVoisines();

    score.textContent = jeu.prota.score.toString();

    if (jeu.estGagne()) {
        msg.textContent = "Gagné !";
        jeu.afficherMines();
    } else if (jeu.estPerdu()) {
        msg.textContent = "Perdu... :(";
        jeu.afficherMines();
    } else {
        msg.textContent = "Nb mines voisines : " + nbMines;
    }

    jeu.prota.majSprite(nbMines);
}


/**
 * Démarre une nouvelle partie
 */
function nouvellePartie() {
    document.getElementById("champ").innerHTML = "";
    
    jeu = new Jeu(0.3);
    
    miseAJour();
}


window.addEventListener("load", function () {
    nouvellePartie()
});
