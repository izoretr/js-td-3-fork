class GameElement {
    constructor(ligne, colonne, spriteURL) {
        this.spriteURL = spriteURL;
        this.spriteElement = document.createElement('img');
        this.spriteElement.setAttribute('src', this.spriteURL);
        this.spriteElement.setAttribute('alt', "object sprite");
        this.spriteElement.classList.add("element");

        this.placer(ligne, colonne);
    }

    /**
     * Déplace l'élément à la position indiquée (et replace le sprite pour qu'il soit affiché au bon endroit)
     * @param ligne {Number} indice de la ligne où placer l'élément
     * @param colonne {Number} indice de la colonne où placer l'élément
     */
    placer(ligne, colonne) {
        this.ligne = ligne;
        this.colonne = colonne;

        const ecartDuBord = 51;
        const tailleCase = 20;

        this.spriteElement.style.top = (ecartDuBord + this.ligne * tailleCase) + "px";
        this.spriteElement.style.left = (ecartDuBord + this.colonne * tailleCase) + "px";
    }

    /**
     * Affiche l'élément
     * Ajoute l'élément (= la balise) dans le <div id="champ">
     */
    afficherDepuis(father) {
        father.appendChild(this.spriteElement);
    }

    /**
     * Cache l'élément
     * Supprime l'élément du <div id="champ">
     */
    cacher() {
        this.spriteElement.remove();
    }

    adjactentA(ligne, colonne) {
        return (Math.abs(this.ligne - ligne) == 1 && Math.abs(this.colonne - colonne) != 1) || (Math.abs(this.ligne - ligne) != 1 && Math.abs(this.colonne - colonne) == 1);
    }
}


class Tresor extends GameElement {
    constructor(colonne) {
        super(0, colonne, 'img/tresor.png');
    }
}


class Mine extends GameElement {
    constructor(ligne, colonne) {
        super(ligne, colonne, 'img/croix.png');
    }
}


class Personnage extends GameElement {
    constructor(colonne) {
        super(19, colonne, 'img/personnage.png');

        this.score = 200;
    }

    /**
     * Exécute un déplacement du joueur horizontalement ou verticalement des valeurs passées en paramètre.
     * Si le déplacement est valide (le joueur ne sort pas de la grille 20x20), la position du personnage est modifiée
     * et le score est décrémenté de 1.
     *
     * Prérequis : exactement un des deux paramètres `dl` et `dc` est non nul, et sa valeur est 1 ou -1.
     * @param dl {Number} déplacement vertical du joueur (modifie la ligne)
     * @param dc {Number} déplacement horizontal du joueur (modifie la colonne)
     */
    deplacer(dl, dc) {
        if (this.ligne + dl < 0 || this.ligne + dl >= 20 || this.colonne + dc < 0 || this.colonne + dc >= 20) {
            return;
        }

        this.placer(this.ligne + dl, this.colonne + dc);
        this.score--;
    }


    /**
     * Met à jour le sprite (= l'image) du personnage
     * On doit afficherDepuis l'image alternative si il y a une mine dans une case voisine
     * @param nbMinesVoisines {Number} nombre de mines dans les cases voisines
     */
    majSprite(nbMinesVoisines) {
        if (nbMinesVoisines) {
            this.spriteURL = 'img/personnage2.png';
            this.spriteElement.setAttribute('src', this.spriteURL);
        } else {
            this.spriteURL = 'img/personnage.png';
            this.spriteElement.setAttribute('src', this.spriteURL);
        }
    }
}