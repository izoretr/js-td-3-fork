class Jeu {
    randomCase() {
        return Math.floor(Math.random() * 20);
    }

    constructor(probaMine) {
        this.prota = new Personnage(this.randomCase());
        this.prota.afficherDepuis(document.getElementById("champ"));

        this.tresor = new Tresor(this.randomCase());
        this.tresor.afficherDepuis(document.getElementById("champ"));

        this.carte = [];
        for (let i = 0; i < 20; i++) {
            this.carte[i] = [];
            for (let j = 0; j < 20; j++) {
                if (this.tresor.adjactentA(i, j) || this.prota.adjactentA(i, j)) continue;

                this.carte[i][j] = Math.random() < probaMine;
            }
        }
    }

    /**
     * Affiche toutes les mines
     */
    afficherMines() {
        const mines = document.createElement('div');
        mines.setAttribute('id', 'minesDiv');
        document.getElementById("champ").appendChild(mines);

        for (let i = 0; i < 20; i++) {
            for (let j = 0; j < 20; j++) {
                if (!this.carte[i][j]) continue;

                const mine = new Mine(i, j);
                mine.afficherDepuis(mines);
            }
        }
    }

    /**
     * Cache toutes les mines
     */
    cacherMines() {
        document.getElementById('minesDiv').remove();
    }

    /**
     * Renvoie le nombre de mines voisines de la position courante du joueur
     * @returns {number} nombre de mines adjacentes à la position du joueur
     */
    nbMinesVoisines() {
        let nombre_de_mines_voisines = 0;

        if (this.prota.ligne+1 < 20) {
            if (this.carte[this.prota.ligne+1][this.prota.colonne] == true) {
                nombre_de_mines_voisines += 1;
            }
        }
        if (this.prota.colonne+1 < 20) {
            if (this.carte[this.prota.ligne][this.prota.colonne+1] == true) {
                nombre_de_mines_voisines += 1;
            }
        }
        if (this.prota.ligne-1 >= 0) {
            if (this.carte[this.prota.ligne-1][this.prota.colonne] == true) {
                nombre_de_mines_voisines += 1;
            }
        }
        if (this.prota.colonne-1 >= 0) {
            if (this.carte[this.prota.ligne][this.prota.colonne-1] == true) {
                nombre_de_mines_voisines += 1;
            }
        }
        
        return nombre_de_mines_voisines;
    }

    /**
     * Indique si le joueur a gagné la partie
     * @returns {boolean} true si le joueur a gagné (position sur le trésor)
     */
    estGagne() {
        return this.prota.ligne === this.tresor.ligne && this.prota.colonne === this.tresor.colonne;
    }

    /**
     * Indique si le joueur a perdu la partie
     * @returns {boolean} true si le joueur est positionné sur une mine ou son score est <= 0
     */
    estPerdu() {
        return this.carte[this.prota.ligne][this.prota.colonne] || this.prota.score <= 0;
    }
}
